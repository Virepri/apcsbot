package apcs.discord.bot;

import co.mide.imgurapi.ImgurApi;
import co.mide.imgurapi.ImgurCallback;
import co.mide.imgurapi.models.*;
import com.google.common.util.concurrent.FutureCallback;
import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.listener.message.MessageCreateListener;
import de.btobastian.javacord.listener.server.ServerJoinListener;
import de.btobastian.javacord.listener.server.ServerMemberAddListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BotMain {

	public static User findUser(String calltag, Collection<User> users) {
		for (User u:users)
		{
			if(u.getMentionTag().equals(calltag))
			{
				return u;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		final DiscordAPI api = Javacord.getApi(args[0], true);

		final ArrayList<BotSettings> bs = new ArrayList<BotSettings>();
		//final ImgurAPI IA = new ImgurAPI("96a3a0d22a1f286","6a63ac9f277d23adfe27f376bf8bbd12333b26cd");
		final ImgurApi Ia = new ImgurApi("96a3a0d22a1f286");
		final BotTags bt = new BotTags();
		//api.connectBlocking();
		api.connect(new FutureCallback<DiscordAPI>() {

			public void onFailure(Throwable t) {
				t.printStackTrace();
				System.err.println("APCSBot failed to connect to discord!");
			}

			public void onSuccess(DiscordAPI arg0) {
				System.out.println("APCSBot succeeded connecting to discord!");

				api.setGame("with a noodle");

				//Commands
				api.registerListener(new MessageCreateListener(){

					public void onMessageCreate(DiscordAPI mapi, final Message mobj) {
						if(mobj.getContent().substring(0, 1).equals("!") && !mobj.getAuthor().isBot())
						{
							//if message starts with !, it's a command.
							String m = mobj.getContent().substring(1);
							String margs = "";
							System.out.println(m);
							if(m.indexOf(' ') != -1)
							{
								margs += m.substring(m.indexOf(' ') + 1);
								m = m.substring(0,m.indexOf(' '));
							}
							m = m.toLowerCase();

							switch(m)
							{
								case "replytest":
									mobj.reply(mobj.getAuthor().getMentionTag());
									break;
                                case "help":
                                    mobj.reply("Available commands in the bot:" +
                                    "\ngit - returns the bitbucket page for this bot. Dig in!" +
                                    "\nping - pong!" +
                                    "\nadmin commands:" +
                                    "\n    admin welcome <boolean> - to welcome, or not to welcome?" +
                                    "\n    admin welcomechannel <string> - sets welcome channel to string" +
                                    "\n    admin setgame <string> sets the game the bot is playing" +
                                    "\n    admin addadmin <string(username)> - adds an admin of supplied username" +
                                    "\njoin - OAuth link" +
                                    "\nhotimages <string> - Searches Imgur's hot images for your query, or displays a sample without a query." +
                                    "\nshowalbum <string> - Displays album with the post id of supplied string" +
                                    "\nshowimage <string> - Same as showalbum, but for individual images." +
                                    "\ntag commands:" +
                                    "\n    tag <string> - Displays stored tag with the name of supplied string" +
                                    "\n    tag create <string(name)> <string(content)> - Pretty self explanatory. Makes a tag with the name & content supplied." +
                                    "\n    tag edit <string(name)> <string(content> - replaces tag of said name with content supplied." +
                                    "\n    tag delete <string(name)> - Deletes tag of supplied name." +
                                    "\ncopypasta - flings a copypasta at you." +
                                    "\nqxczv - Displays a link to a err.... Interesting place." +
                                    "\nmeme - I wish there was a clever way to make this an easter egg.");
                                    break;
                                case "qxczv":
                                    mobj.reply("https://qxczv.herokuapp.com");
                                    break;
                                case "copypasta":
                                    mobj.reply(BotMemes.getCopypasta());
                                    break;
                                case "meme":
                                    mobj.reply(BotMemes.getMeme());
                                    break;
								case "git":
                                    mobj.reply("https://www.bitbucket.org/virepri/apcsbot" + margs);
									break;
								case "ping":
									mobj.reply("Pong!");
									break;
								case "admin":
									Server svr = mobj.getChannelReceiver().getServer();
									int svrid = 0;
									for(int x = 0;x > bs.size();x++)
									{
										if(bs.get(x).servername.equals(svr.getId()))
										{
											svrid = x;
										}
									}
                                    if(bs.get(svrid).admins.size() == 0){
                                        bs.get(svrid).admins.add(mobj.getAuthor());
                                        mobj.reply(mobj.getAuthor().getMentionTag() + " has claimed bot admin.");
                                    }
									if(bs.get(svrid).findAdmin(mobj.getAuthor()))
									{
										String acttype1 = margs.substring(0,margs.indexOf(' '));
										acttype1 = acttype1.toLowerCase();
										margs = margs.substring(margs.indexOf(' ') + 1);
										switch(acttype1)
										{
											case "welcome":
												if(margs.equalsIgnoreCase("true"))
												{
													if(bs.get(svrid).servername.equals(svr.getId()))
													{
														bs.get(svrid).welcome = true;
													}
												}
												else
												{

													if(bs.get(svrid).servername.equals(svr.getId()))
													{
														bs.get(svrid).welcome = false;
													}
												}
												break;
											case "welcomechannel":
												bs.get(svrid).welcomeChannel = margs;
												break;
											case "addadmin":
												User u = findUser(margs,api.getUsers());
												if(u != null) {
													bs.get(svrid).admins.add(u);
													mobj.reply("Added " + u.getMentionTag() + " to admins list for this server");
												}
												else
													mobj.reply("Could not find " + margs + " in the user list!");
												break;
											case "setgame":
												api.setGame(margs);
												break;
										}
									}
									break;
								case "join":
									mobj.reply("This bot doesn't use discord invites; please visit https://discordapp.com/oauth2/authorize?client_id=186758092195627009&scope=bot&permissions=0");
                                    break;
								case "hotimages":
									final String mf = margs;
										Ia.getHot(new ImgurCallback<ImgurGallery>(){
											public void onError(String arg0) {
												// TODO Auto-generated method stub
												mobj.reply("Sorry! We're having a hard time connecting to Imgur right now. Try again in a moment.");
											}
											public void onResponse(ImgurGallery IG) {
												// TODO Auto-generated method stub
												List<ImgurGalleryData> d = IG.getData();
												if(mf.length() == 0)
												{
													for(ImgurGalleryData galleryData: d){
														mobj.reply("Title - " + galleryData.getTitle() + " - URL - " + "https://imgur.com/gallery/" + galleryData.getId());

													}
												}
												else
												{
													for(ImgurGalleryData galleryData:d)
													{
														if(galleryData.getTitle().contains(mf))
														{
															mobj.reply("Title - " + galleryData.getTitle() + " - URL - " + "https://imgur.com/gallery/" + galleryData.getId());
														}
													}
												}
											}});
									break;
								case "showalbum":
									System.out.println(margs);
									Ia.getAlbum(margs, new ImgurCallback<ImgurAlbum>(){

										public void onError(String arg0) {
											// TODO Auto-generated method stub
											System.out.println(arg0);
											mobj.reply("Sorry! We're having a hard time connecting to Imgur right now. Try again in a moment.");
											mobj.reply("Please check that your album ID is correct and try again.");
										}

										public void onResponse(ImgurAlbum IAl) {
											// TODO Auto-generated method stub
											ImgurAlbumData d = IAl.getData();
											final List<? extends ImgurImageData> id = d.getImages();
											mobj.reply("Album Title - " + d.getTitle());
											mobj.reply("Album size - " + id.size() + " images.");
											mobj.reply("Album Sample: ");
											for(ImgurImageData iid:id)
											{
												mobj.reply(iid.getLink());
												try {
													TimeUnit.SECONDS.sleep((long) 0.5);
												} catch (InterruptedException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											}
										}});
									break;
								case "showimage":
									Ia.getImage(margs, new ImgurCallback<ImgurImage>(){

										public void onError(String arg0) {
											// TODO Auto-generated method stub

										}

										public void onResponse(ImgurImage arg0) {
											// TODO Auto-generated method stub
											ImgurImageData iid = arg0.getData();
											mobj.reply("Image Title: " + iid.getTitle());
											mobj.reply(iid.getLink());
										}});
									break;
								case "tag":
									if(margs.indexOf(' ') == -1)
									{
										if(bt.FindTag(margs) == -1)
										{
											mobj.reply("Unable to find tag");
										}
										else
										{
											mobj.reply(bt.tags.get(bt.FindTag(margs)).get(1));
										}
									}
									else
									{
										//margs = margs.substring(margs.indexOf(' '));
										String acttype = margs.substring(0,margs.indexOf(' '));
										acttype = acttype.toLowerCase();
										margs = margs.substring(margs.indexOf(' ') + 1);
										String name = margs.substring(0,margs.indexOf(' '));
										margs = margs.substring(margs.indexOf(' ') + 1);
										switch(acttype)
										{
										case "create":
											if(bt.FindTag(name) == -1)
											{
												ArrayList<String> tag = new ArrayList<String>();
												tag.add(name);
												tag.add(margs);
												bt.tags.add(tag);
												mobj.reply("Created tag " + name);
											}
											else
											{
												mobj.reply("Tag exists!");
											}
											break;
										case "edit":
											bt.tags.get(bt.FindTag(name)).set(1, margs);
											mobj.reply("Edited tag " + name);
											break;
										case "delete":
											bt.tags.remove(bt.FindTag(name));
											mobj.reply("Deleted " + name);
											break;
										default:
											mobj.reply("Incorrect usage! ex. !tag <create|edit|name> <name> <content>; !tag create test lel; !tag edit test lol; !tag test");
										}
									}
								break;
							}
						}
					}});

				//Welcomer
				api.registerListener(new ServerJoinListener(){

					public void onServerJoin(DiscordAPI usr, Server svr) {
						/*if(bs.welcome)
						{
							svr.getChannelById(bs.welcomeChannel).sendMessage("Welcome, " + usr.getYourself().getName());
						}*/
						bs.add(new BotSettings(svr.getId()));
						System.out.println("Joined server " + svr.getName());
					}});

				api.registerListener(new ServerMemberAddListener(){
					public void onServerMemberAdd(DiscordAPI arg0, User usr, Server svr) {
						for(int x = 0;x > bs.size();x++)
						{
							if(bs.get(x).servername.equals(svr.getId()) && bs.get(x).welcome)
							{
								svr.getChannelById(bs.get(x).welcomeChannel).sendMessage("Welcome, " + usr.getName() + " to the " + svr.getName() + " Discord Server!");
							}
						}
					}});
			}

			});
	}

}
