package apcs.discord.bot;

import java.util.ArrayList;

public class BotTags {
	public ArrayList<ArrayList<String>> tags = new ArrayList<ArrayList<String>>();
	
	public BotTags(){}
	
	public int FindTag(String name){
		int i = 0;
		for(ArrayList<String> tag:tags)
		{
			if(tag.get(0).equals(name))
			{
				return i;
			}
			i++;
		}
		return -1;
	}
}
