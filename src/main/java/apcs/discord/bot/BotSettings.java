package apcs.discord.bot;

import de.btobastian.javacord.entities.User;

import java.util.ArrayList;

public class BotSettings {
	boolean censor = false;
	boolean welcome = true;
	String welcomeChannel = "General";
	ArrayList<User> admins = new ArrayList<User>();
	String servername;
	
	public BotSettings(String sname)
	{
		servername = sname;
	}
	
	public boolean findAdmin(User u)
	{
		for(User n:admins)
		{
			if(n.getMentionTag().equals(u.getMentionTag()))
			{
				return true;
			}
		}
		return false;
	}
}
